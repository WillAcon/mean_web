'use strict';

module.exports = function (grunt) {
    grunt.initConfig({
        clean: ["dist", '.tmp'],
        copy: {
            main: {
                expand: true,
                cwd: 'app/',
                src: ['**', '!js/**', '!lib/**', '!**/*.css','!scripts/**','!styles/**','!modules/**','!bower_components/**','modules/*/views/**'],
                dest: 'dist/'
            },
            fonts: {
                expand: true,
                flatten: true,
                cwd: 'app/',
                src: ['**/*.woff','**/*.woff2','**/*.ttf','**/*.svg','**/*.eot'],
                dest: 'dist/assets/css/fonts'
            },
        },
        rev: {
            files: {
                src: ['dist/**/*.{js,css}']
            }
        },
        useminPrepare: {
            html: 'app/index.html'
        },
        usemin: {
            html: 'dist/index.html'
        },
        uglify: {
            options: {
                report: 'min',
                mangle: false
            }
        },
        htmlmin: {
           dist: {
              options: {
                 removeComments: true,
                 collapseWhitespace: true
              },
              files: [{
                 expand: true,
                 cwd: 'dist',
                 src: ['modules/**/*.html', '*.html', '**/*.html'],
                 dest: 'dist/'
              }]
           }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-rev');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');

    // Tell Grunt what to do when we type "grunt" into the terminal
    grunt.registerTask('default', [
        'copy', 'useminPrepare', 'concat', 'uglify', 'cssmin', 'rev', 'usemin', 'htmlmin'
    ]);
};