(function () {
  'use strict';
  angular.module('App', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngMessages',
    'ngAnimate',
    'toastr',
    'ui.router',
    'satellizer'
  ]).value('nickName', 'anonymous');
}());
(function () {
  'use strict';
  angular.module('App').config([
    '$stateProvider',
    '$urlRouterProvider',
    '$authProvider',
    function ($stateProvider, $urlRouterProvider, $authProvider) {
      $stateProvider.state('home', {
        url: '/',
        controller: 'HomeCtrl',
        templateUrl: 'views/login.html'
      }).state('login', {
        url: '/login',
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        resolve: { skipIfLoggedIn: skipIfLoggedIn }
      }).state('signup', {
        url: '/signup',
        templateUrl: 'views/signup.html',
        controller: 'SignupCtrl',
        resolve: { skipIfLoggedIn: skipIfLoggedIn }
      }).state('logout', {
        url: '/logout',
        template: null,
        controller: 'LogoutCtrl'
      }).state('profile', {
        url: '/profile',
        templateUrl: 'views/profile.html',
        controller: 'ProfileCtrl',
        resolve: { loginRequired: loginRequired }
      });
      $urlRouterProvider.otherwise('/login');
      $authProvider.facebook({ clientId: '464053600441398' });
      $authProvider.twitter({ url: '/auth/twitter' });
      $authProvider.oauth2({
        name: 'foursquare',
        url: '/auth/foursquare',
        clientId: 'MTCEJ3NGW2PNNB31WOSBFDSAD4MTHYVAZ1UKIULXZ2CVFC2K',
        redirectUri: window.location.origin || window.location.protocol + '//' + window.location.host,
        authorizationEndpoint: 'https://foursquare.com/oauth2/authenticate'
      });
      function skipIfLoggedIn($q, $auth) {
        var deferred = $q.defer();
        if ($auth.isAuthenticated()) {
          deferred.reject();
        } else {
          deferred.resolve();
        }
        return deferred.promise;
      }
      function loginRequired($q, $location, $auth) {
        var deferred = $q.defer();
        if ($auth.isAuthenticated()) {
          deferred.resolve();
        } else {
          $location.path('/login');
        }
        return deferred.promise;
      }
    }
  ]);
}());
(function () {
  'use strict';
  angular.module('App').directive('passwordMatch', function () {
    return {
      require: 'ngModel',
      scope: { otherModelValue: '=passwordMatch' },
      link: function (scope, element, attributes, ngModel) {
        ngModel.$validators.compareTo = function (modelValue) {
          return modelValue === scope.otherModelValue;
        };
        scope.$watch('otherModelValue', function () {
          ngModel.$validate();
        });
      }
    };
  });
}());
(function () {
  'use strict';
  angular.module('App').directive('passwordStrength', function () {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function (scope, element, attrs, ngModel) {
        var indicator = element.children();
        var dots = Array.prototype.slice.call(indicator.children());
        var weakest = dots.slice(-1)[0];
        var weak = dots.slice(-2);
        var strong = dots.slice(-3);
        var strongest = dots.slice(-4);
        element.after(indicator);
        element.bind('keyup', function () {
          var matches = {
              positive: {},
              negative: {}
            };
          var counts = {
              positive: {},
              negative: {}
            };
          var tmp, strength = 0, letters = 'abcdefghijklmnopqrstuvwxyz', numbers = '01234567890', symbols = '\\!@#$%&/()=?\xbf', strValue;
          angular.forEach(dots, function (el) {
            el.style.backgroundColor = '#ebeef1';
          });
          if (ngModel.$viewValue) {
            // Increase strength level
            matches.positive.lower = ngModel.$viewValue.match(/[a-z]/g);
            matches.positive.upper = ngModel.$viewValue.match(/[A-Z]/g);
            matches.positive.numbers = ngModel.$viewValue.match(/\d/g);
            matches.positive.symbols = ngModel.$viewValue.match(/[$-\/:-?{-~!^_`\[\]]/g);
            matches.positive.middleNumber = ngModel.$viewValue.slice(1, -1).match(/\d/g);
            matches.positive.middleSymbol = ngModel.$viewValue.slice(1, -1).match(/[$-\/:-?{-~!^_`\[\]]/g);
            counts.positive.lower = matches.positive.lower ? matches.positive.lower.length : 0;
            counts.positive.upper = matches.positive.upper ? matches.positive.upper.length : 0;
            counts.positive.numbers = matches.positive.numbers ? matches.positive.numbers.length : 0;
            counts.positive.symbols = matches.positive.symbols ? matches.positive.symbols.length : 0;
            counts.positive.numChars = ngModel.$viewValue.length;
            tmp += counts.positive.numChars >= 8 ? 1 : 0;
            counts.positive.requirements = tmp >= 3 ? tmp : 0;
            counts.positive.middleNumber = matches.positive.middleNumber ? matches.positive.middleNumber.length : 0;
            counts.positive.middleSymbol = matches.positive.middleSymbol ? matches.positive.middleSymbol.length : 0;
            // Decrease strength level
            matches.negative.consecLower = ngModel.$viewValue.match(/(?=([a-z]{2}))/g);
            matches.negative.consecUpper = ngModel.$viewValue.match(/(?=([A-Z]{2}))/g);
            matches.negative.consecNumbers = ngModel.$viewValue.match(/(?=(\d{2}))/g);
            matches.negative.onlyNumbers = ngModel.$viewValue.match(/^[0-9]*$/g);
            matches.negative.onlyLetters = ngModel.$viewValue.match(/^([a-z]|[A-Z])*$/g);
            counts.negative.consecLower = matches.negative.consecLower ? matches.negative.consecLower.length : 0;
            counts.negative.consecUpper = matches.negative.consecUpper ? matches.negative.consecUpper.length : 0;
            counts.negative.consecNumbers = matches.negative.consecNumbers ? matches.negative.consecNumbers.length : 0;
            // Calculations
            strength += counts.positive.numChars * 4;
            if (counts.positive.upper) {
              strength += (counts.positive.numChars - counts.positive.upper) * 2;
            }
            if (counts.positive.lower) {
              strength += (counts.positive.numChars - counts.positive.lower) * 2;
            }
            if (counts.positive.upper || counts.positive.lower) {
              strength += counts.positive.numbers * 4;
            }
            strength += counts.positive.symbols * 6;
            strength += (counts.positive.middleSymbol + counts.positive.middleNumber) * 2;
            strength += counts.positive.requirements * 2;
            strength -= counts.negative.consecLower * 2;
            strength -= counts.negative.consecUpper * 2;
            strength -= counts.negative.consecNumbers * 2;
            if (matches.negative.onlyNumbers) {
              strength -= counts.positive.numChars;
            }
            if (matches.negative.onlyLetters) {
              strength -= counts.positive.numChars;
            }
            strength = Math.max(0, Math.min(100, Math.round(strength)));
            if (strength > 85) {
              angular.forEach(strongest, function (el) {
                el.style.backgroundColor = '#008cdd';
              });
            } else if (strength > 65) {
              angular.forEach(strong, function (el) {
                el.style.backgroundColor = '#6ead09';
              });
            } else if (strength > 30) {
              angular.forEach(weak, function (el) {
                el.style.backgroundColor = '#e09115';
              });
            } else {
              weakest.style.backgroundColor = '#e01414';
            }
          }
        });
      },
      template: '<span class="password-strength-indicator"><span></span><span></span><span></span><span></span></span>'
    };
  });
}());
(function () {
  'use strict';
  angular.module('App').controller('HomeCtrl', [
    '$scope',
    '$http',
    function ($scope, $http) {
      $http.jsonp('https://api.github.com/repos/sahat/satellizer?callback=JSON_CALLBACK').success(function (data) {
        if (data) {
          if (data.data.stargazers_count) {
            $scope.stars = data.data.stargazers_count;
          }
          if (data.data.forks) {
            $scope.forks = data.data.forks;
          }
          if (data.data.open_issues) {
            $scope.issues = data.data.open_issues;
          }
        }
      });
    }
  ]);
}());
(function () {
  'use strict';
  angular.module('App').controller('LoginCtrl', [
    '$scope',
    '$location',
    '$auth',
    'toastr',
    function ($scope, $location, $auth, toastr) {
      $scope.login = function () {
        $auth.login($scope.user).then(function () {
          toastr.success('You have successfully signed in!');
          $location.path('/');
        }).catch(function (error) {
          toastr.error(error.data.message, error.status);
        });
      };
      $scope.authenticate = function (provider) {
        $auth.authenticate(provider).then(function () {
          toastr.success('You have successfully signed in with ' + provider + '!');
          $location.path('/');
        }).catch(function (error) {
          if (error.error) {
            // Popup error - invalid redirect_uri, pressed cancel button, etc.
            toastr.error(error.error);
          } else if (error.data) {
            // HTTP response error from server
            toastr.error(error.data.message, error.status);
          } else {
            toastr.error(error);
          }
        });
      };
    }
  ]);
}());
(function () {
  'use strict';
  angular.module('App').controller('SignupCtrl', [
    '$scope',
    '$location',
    '$auth',
    'toastr',
    function ($scope, $location, $auth, toastr) {
      $scope.signup = function () {
        $auth.signup($scope.user).then(function (response) {
          $auth.setToken(response);
          $location.path('/');
          toastr.info('You have successfully created a new account and have been signed-in');
        }).catch(function (response) {
          toastr.error(response.data.message);
        });
      };
    }
  ]);
}());
(function () {
  'use strict';
  angular.module('App').controller('LogoutCtrl', [
    '$location',
    '$auth',
    'toastr',
    function ($location, $auth, toastr) {
      if (!$auth.isAuthenticated()) {
        return;
      }
      $auth.logout().then(function () {
        toastr.info('You have been logged out');
        $location.path('/');
      });
    }
  ]);
}());
(function () {
  'use strict';
  angular.module('App').controller('ProfileCtrl', [
    '$scope',
    '$auth',
    'toastr',
    'Account',
    function ($scope, $auth, toastr, Account) {
      $scope.getProfile = function () {
        Account.getProfile().then(function (response) {
          $scope.user = response.data;
        }).catch(function (response) {
          toastr.error(response.data.message, response.status);
        });
      };
      $scope.updateProfile = function () {
        Account.updateProfile($scope.user).then(function () {
          toastr.success('Profile has been updated');
        }).catch(function (response) {
          toastr.error(response.data.message, response.status);
        });
      };
      $scope.link = function (provider) {
        $auth.link(provider).then(function () {
          toastr.success('You have successfully linked a ' + provider + ' account');
          $scope.getProfile();
        }).catch(function (response) {
          toastr.error(response.data.message, response.status);
        });
      };
      $scope.unlink = function (provider) {
        $auth.unlink(provider).then(function () {
          toastr.info('You have unlinked a ' + provider + ' account');
          $scope.getProfile();
        }).catch(function (response) {
          toastr.error(response.data ? response.data.message : 'Could not unlink ' + provider + ' account', response.status);
        });
      };
      $scope.getProfile();
    }
  ]);
}());
(function () {
  'use strict';
  angular.module('App').controller('NavbarCtrl', [
    '$scope',
    '$auth',
    function ($scope, $auth) {
      $scope.isAuthenticated = function () {
        return $auth.isAuthenticated();
      };
    }
  ]);
}());
(function () {
  'use strict';
  angular.module('App').factory('Account', [
    '$http',
    function ($http) {
      return {
        getProfile: function () {
          return $http.get('/api/me');
        },
        updateProfile: function (profileData) {
          return $http.put('/api/me', profileData);
        }
      };
    }
  ]);
}());