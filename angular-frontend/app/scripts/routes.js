(function() {

  'use strict';
  angular.module('App')
    .config(function($stateProvider, $locationProvider, $urlRouterProvider) {

      /*$stateProvider
        .state('home', {
          url: '/',
          views:{
            '': {
              templateUrl: 'views/home.html',
              controller: 'HomeCtrl'
             },
             'header': {
                            templateUrl: 'views/header.html',
                            controller: 'NavbarCtrl'
                        }
          }
        });*/
      $stateProvider.state('app', {
        abstract: true,
        views: {
           header: {
             templateUrl: '/views/header.html',
             controller: 'NavbarCtrl'
           },
           'main': {
             templateUrl: '/views/home.html',
             controller: 'HomeCtrl'
           },
           
          /* footer: {
             templateUrl: 'footer.html',
             controller: 'FooterController as Footer'
           }*/
        }
    })
    .state('app.init', {
      url: '/',
      templateUrl: 'views/home_content.html',
      controller: 'HomeCtrl'
    })
      //other whens removed
      .state('app.otherwise',{
         url: "*path",
         templateUrl: '/views/404.tpl.html',
         controller: '404'
      });

      $locationProvider.html5Mode(true);
      //$urlRouterProvider.otherwise('/404');

     /* $authProvider.facebook({
        //clientId: '464053600441398'
      });


      $authProvider.twitter({
        url: '/auth/twitter'
      });

      $authProvider.oauth2({
        name: 'foursquare',
        url: '/auth/foursquare',
        clientId: 'MTCEJ3NGW2PNNB31WOSBFDSAD4MTHYVAZ1UKIULXZ2CVFC2K',
        redirectUri: window.location.origin || window.location.protocol + '//' + window.location.host,
        authorizationEndpoint: 'https://foursquare.com/oauth2/authenticate'
      });
*/
      function skipIfLoggedIn($q, $auth) {
        var deferred = $q.defer();
        if ($auth.isAuthenticated()) {
          deferred.reject();
        } else {
          deferred.resolve();
        }
        return deferred.promise;
      }

      function loginRequired($q, $location, $auth) {
        var deferred = $q.defer();
        if ($auth.isAuthenticated()) {
          deferred.resolve();
        } else {
          $location.path('/login');
        }
        return deferred.promise;
      }
      
    });

}());