(function () {
  'use strict';
  /**
   * @ngdoc overview
   * @name loopbackApp
   * @description
   * # loopbackApp
   *
   * Main module of the application.
   */
  angular
    .module('App', [
      'lumx',
      'ngCookies',
      'ngResource',
      'ngSanitize',
      'ngMessages',
      'ngAnimate',
      'ngRoute',
      'toastr',
      //'cloudinary',//Image Upload http://cloudinary.com/
      'ui.router',
      'gettext',
      'btford.socket-io',
      'satellizer',
      'module.admin',
      'module.users',
      'module.upload'
    ])
    .run(function ($rootScope, $cookies) {

      

    });
    

})();
