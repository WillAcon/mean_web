(function() {

  'use strict';
  angular.module('App')
    .factory('CONFIG', function($http) {
      return {
        Client_id: function() {
          $http.get('/config/data').getProfile()
          .then(function(response) {
            console.log(response);
            //$scope.user = response.data;
            return response.client_id;
          })
          .catch(function(response) {
            //console.log('No autorizado');
            //toastr.error(response.data.message, response.status);
          });         
        }
      };
    });

}());