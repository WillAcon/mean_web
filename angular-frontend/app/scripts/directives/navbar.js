(function () {
  'use strict';
  /**
   * @ngdoc directive
   * @name mean.module.core.directive:login
   * @description
   * # login
   */
  angular
    .module('App')
    .directive('menumain', function () {
      return {
        templateUrl: 'views/menu.html',
        restrict: 'E'
      };
    });

})();
