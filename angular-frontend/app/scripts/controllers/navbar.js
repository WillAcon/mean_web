(function() {

	'use strict';
	angular.module('App')
	  .controller('NavbarCtrl', function($scope, $auth) {
	    $scope.isAuthenticated = function() {
	      return $auth.isAuthenticated();
	    };
	    $scope.palmNavIsOpen = false;
	    $scope.togglePalmNav = function(){
            $scope.palmNavIsOpen = !$scope.palmNavIsOpen;
            console.log($scope.palmNavIsOpen);
        }
        
	  });

}());