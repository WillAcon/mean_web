(function () {
  'use strict';
   angular.module('module.upload', [
  		'cloudinary',
  		'ngFileUpload',
  		'infinite-scroll'
  	]);

})();
