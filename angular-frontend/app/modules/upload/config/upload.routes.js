(function () {
  'use strict';
  angular
    .module('module.upload')
    .config(function ($stateProvider, $authProvider) {
      $stateProvider
        .state('app.files-create', {
          url: '/files-create',
          templateUrl: 'modules/upload/views/create.html',
          controller: 'UploadLoadCtrl',
          resolve: {
            loginRequired: loginRequired
          }
        })
        .state('app.files-list', {
            url: '/files-list',
            templateUrl: 'modules/upload/views/list.html',
            controller: 'UploadCtrl',
            resolve: {
                  loginRequired: loginRequired
            }
        })
        .state('editar', {
        url: "/office/:officeId/edit",
        templateUrl: 'modules/offices/views/edit.html',
        controller: 'OfficeUpdateCtrl',
        resolve: {
            loginRequired: loginRequired
        }
       });
       
        function loginRequired($q, $location, $auth) {
          var deferred = $q.defer();
          if ($auth.isAuthenticated()) {
            deferred.resolve();
          } else {
            $location.path('/login');
          }
          return deferred.promise;
        }
    });
})();
