(function() {
  'use strict';
  /**
   * @ngdoc function
   * @name mean.module.users.controller:RegisterCtrl
   * @description Login Controller
   * @requires $scope
   * @requires $auth
   * @requires toastr
   * @requires Account
   * Controller for Register Page
   **/
  angular
    .module('module.upload')
    .controller('UploadLoadCtrl', function($scope, $rootScope, $auth, toastr, Account, Upload, socket, Photos) {
      //console.log(Office.getOffice());
      console.log('upload');

      $scope.reddit = new Photos();
      console.log($scope.reddit);
     /* socket.on('list', function (data) {
          $rootScope.photos = data.photos;
      });
      socket.on('guardarfoto', function (data) {
        // $scope.messages.push(message);
        $rootScope.photos.push(data.photo);
      });*/

      socket.on('updated-foto', function(data) {
          //$scope.photos.push(data);
          $scope.reddit.items.push(data);
          console.log($scope.reddit.items);
       });


      $scope.uploadFiles = function(files) {
        console.log(files);

        $scope.files = files;
        if (!$scope.files) return;

         /*UploadService.setUploadFile($scope.files)
          .then(function(response) {
            
            console.log(response);

          })
          .catch(function(response) {
            toastr.error('Error', response.status);
          });*/
        angular.forEach(files, function(file) {
          if (file && !file.$error) {
            file.upload = Upload.upload({
              url: "/photo/create",
              data: {
                tags: 'myphotoalbum',
                file: file,
                options: {
                  "alt": $scope.title,
                  "caption": $scope.title
                }
              }
            }).progress(function (e) {
                file.progress = Math.round((e.loaded * 100.0) / e.total);
                file.status = "Uploading... " + file.progress + "%";
            }).success(function (data, status, headers, config) {
             // $scope.photos = $scope.photos || [];
                delete data["__v"];
                socket.emit('guardar_foto', data);
            }).error(function (data, status, headers, config) {
                file.result = data;
            });
          }
        });
      };

      /* Modify the look and fill of the dropzone when files are being dragged over it */
      $scope.dragOverClass = function($event) {
        var items = $event.dataTransfer.items;
        var hasFile = false;
        if (items != null) {
          for (var i = 0 ; i < items.length; i++) {
            if (items[i].kind == 'file') {
              hasFile = true;
              break;
            }
          }
        } else {
          hasFile = true;
        }
        return hasFile ? "dragover" : "dragover-err";
      };
     
  

    });

}());