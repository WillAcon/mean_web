(function() {

  'use strict';
  angular.module('module.upload')
    .factory('UploadService' , function($http, $resource, cloudinary, socketFactory) {
      //console.log('ok');
      return {
        /*SetFiles: function(files) {
            $scope.files = files;
          if (!$scope.files) return;
          angular.forEach(files, function(file, title, tag) {
            if (file && !file.$error) {
              file.upload = $upload.upload({
                url: "https://api.cloudinary.com/v1_1/" + cloudinary.config().cloud_name + "/upload",
                data: {
                  upload_preset: cloudinary.config().upload_preset,
                  tags: tags,
                  context: 'photo=' + title,
                  file: file
                }
              }).progress(function (e) {
                file.progress = Math.round((e.loaded * 100.0) / e.total);
                file.status = "Uploading... " + file.progress + "%";
              }).success(function (data, status, headers, config) {
                $rootScope.photos = $rootScope.photos || [];
                data.context = {custom: {photo: $scope.title}};
                file.result = data;
                $rootScope.photos.push(data);
              }).error(function (data, status, headers, config) {
                file.result = data;
              });
            }
          });
        },*/
        getListFiles: function(params) {
           /*   var url = cloudinary.url(tags, {format: 'json', type: 'list'});
              //cache bust
              url = url + "?" + Math.ceil(new Date().getTime()/1000);
              console.log(url);
   
              console.log($http);
             // delete $http.defaults.headers.common['Authorization'];

              // return $http({method:'GET',headers: { 'Authorization': ''}, url: url});

             return $http.get(url, {}, {
                  headers: {
                      'Authorization': null
                  }
              });*/

        


             /* var socket = socketFactory();
              socket.forward('broadcast');
              socket.emit('list', 'nickName', '$scope.message');
              //$scope.message = '';*/
              if(!params) {
                params = { limit: 10, skip: 0 };
              }
             return $http.get('/photo/list',{ params: params});
        },
        getFileId: function(id) {
              var url = cloudinary.url(id, {format: 'json', type: 'list'});
              //cache bust
              url = url + "?" + Math.ceil(new Date().getTime()/1000);
              return $resource(url, {}, {
                photo: {method:'GET', isArray:false}
              });
        },

/*        getSocket() {
            var socket = socketFactory();
            socket.forward('broadcast');
            return socket;
        },*/

        setUploadFile: function(filesData) {
           return $http.post('/photo/create', filesData);
        }


      };
    }).
    factory('socket', function($rootScope, socketFactory) {
 		      var socket = io.connect();

          var wrappedSocket = socketFactory({
            ioSocket: socket
          });

          wrappedSocket.reconnect = function() {
            if(socket.socket.connected) {
              socket.socket.disconnect();
              socket.socket.connect();
            } else {
              socket.socket.connect();
            }
          };

          wrappedSocket.disconnect = function() {
            socket.socket.disconnect();
          };

          wrappedSocket.connect = function() {
            socket.socket.connect();
          };

         return wrappedSocket;

    }).
    factory('Photos', function($http) {
        var Photos = function() {
          this.items = [];
          this.busy = false;
          this.skip = 0;
          this.limit = 10;
        };

        Photos.prototype.nextPage = function() {
          if (this.busy) return;
          this.busy = true;
          var url = "/photo/list?skip=" + this.skip + "&limit=" + this.limit + "&callback=JSON_CALLBACK";
          $http.jsonp(url).success(function(data) {
            if(data.length > 0) {
                  this.items = this.items.concat(data);
                  /*for (var i = 0; i < data.length; i++) {
                    this.items.push(data[i]);
                  }*/
                  this.skip = this.items.length;
                  this.busy = false;
               }else {
                  this.busy = true;
               }
          }.bind(this));
        };
     return Photos;
    });


}());
