(function () {
  'use strict';
  /**
   * @ngdoc directive
   * @name mean.module.core.directive:register
   * @description
   * # register
   */
  angular
    .module('module.users')
    .directive('register', function () {
      return {
        templateUrl: 'modules/users/views/register.html',
        restrict: 'E'
      };
    });

})();
