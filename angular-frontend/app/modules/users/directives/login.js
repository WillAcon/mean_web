(function () {
  'use strict';
  /**
   * @ngdoc directive
   * @name mean.module.core.directive:login
   * @description
   * # login
   */
  angular
    .module('module.users')
    .directive('login', function () {
      return {
        templateUrl: 'modules/users/views/login.html',
        restrict: 'E'
      };
    });

})();
