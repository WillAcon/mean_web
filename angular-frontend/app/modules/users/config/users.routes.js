(function () {
  'use strict';
  angular
    .module('module.users')
    .config(function ($stateProvider, $authProvider) {
      //var client_id = Account.Client_id();

      $stateProvider.state('user', {
        abstract: true,
        views: {
           header: {
             templateUrl: 'views/header.html',
             controller: 'NavbarCtrl'
           },
           'main': {
             templateUrl: '/views/home.html',
             controller: 'HomeCtrl'
           },
          /* footer: {
             templateUrl: 'footer.html',
             controller: 'FooterController as Footer'
           }*/
        }
      })
        .state('app.login', {
          url: '/login',
        /*  views: {
           'main': {
             template: '<login></login>',
             controller: 'LoginCtrl'
           }
          },*/
          template: '<login></login>',
          controller: 'LoginCtrl',
          resolve: {
            skipIfLoggedIn: skipIfLoggedIn
          }
        })
        .state('app.signup', {
          url: '/signup',
          template: '<register></register>',
          controller: 'SignupCtrl',
          resolve: {
            skipIfLoggedIn: skipIfLoggedIn
          }
        })
        .state('demo', {
            url: '/demo',
            abstract:true,
            templateUrl: 'modules/users/views/test.html'
        })
        .state('demo/prueba', {
          url: '/demo/prueba',
         // templateUrl: 'modules/users/views/test.html',
          //controller: 'UserCtrl',
          views: {
            'filters': { 
              templateUrl: 'modules/users/views/test.html',
            },
             'main': { 
              templateUrl: 'modules/users/views/test1.html',
            },
          }
        })
        .state('app.logout', {
          url: '/logout',
          template: null,
          controller: 'LogoutCtrl'
        })
        .state('app.profile', {
          url: '/profile',
          templateUrl: 'modules/users/views/profile.html',
          controller: 'ProfileCtrl',
          resolve: {
            loginRequired: loginRequired
          }
        })
        .state('admin/users', {
          url: '/admin/users',
          templateUrl: 'modules/users/views/list.html',
          controller: 'UserCtrl',
          resolve: {
            loginRequired: loginRequired
          }
        })
        .state('app.users', {
          abstract: true,
          url: '/users',
          templateUrl: 'modules/users/views/main.html'
        })
        .state('app.users.list', {
          url: '',
          templateUrl: 'modules/users/views/list.html',
          controllerAs: 'ctrl',
          controller: function (users) {
            console.log('users', users);
            this.users = users;
          },
          resolve: {
            users: function (UserService) {
              console.log('users');
              return UserService.find();
            }
          }
        })
        .state('app.users.add', {
          url: '/add',
          templateUrl: 'modules/users/views/form.html',
          controllerAs: 'ctrl',
          controller: function ($state, UserService, user) {
            this.user = user;
            this.formFields = UserService.getFormFields('add');
            this.formOptions = {};
            this.submit = function () {
              UserService.upsert(this.user).then(function () {
                $state.go('^.list');
              }).catch(function (err) {
                console.log(err);
              });
            };
          },
          resolve: {
            user: function () {
              return {};
            }
          }
        })
        .state('app.users.edit', {
          url: '/edit/:id',
          templateUrl: 'modules/users/views/form.html',
          controllerAs: 'ctrl',
          controller: function ($state, UserService, user) {
            this.user = user;
            this.formFields = UserService.getFormFields('edit');
            this.formOptions = {};
            this.submit = function () {
              UserService.upsert(this.user).then(function () {
                $state.go('^.list');
              });
            };
          },
          resolve: {
            user: function ($stateParams, UserService) {
              return UserService.findById($stateParams.id);
            }
          }
        })
        .state('app.users.view', {
          url: '/view/:id',
          templateUrl: 'modules/users/views/view.html',
          controllerAs: 'ctrl',
          controller: function (user) {
            this.user = user;
          },
          resolve: {
            user: function ($stateParams, UserService) {
              return UserService.findById($stateParams.id);
            }
          }
        })
        .state('app.users.delete', {
          url: '/:id/delete',
          template: '',
          controller: function ($stateParams, $state, UserService) {
            UserService.delete($stateParams.id, function () {
              $state.go('^.list');
            }, function () {
              $state.go('^.list');
            });
          }
        })
        .state('app.users.profile', {
          url: '/profile',
          templateUrl: 'modules/users/views/profile.html',
          controllerAs: 'ctrl',
          controller: function ($state, UserService, user) {
            this.user = user;
            this.formFields = UserService.getFormFields('edit');
            this.formOptions = {};
            this.submit = function () {
              UserService.upsert(this.user).then(function () {
                $state.go('^.profile');
              });
            };
          },
          resolve: {
            user: function (User) {
              return User.getCurrent(function (user) {
                return user;
              }, function (err) {
                console.log(err);
              });
            }
          }
        });

        $authProvider.facebook({
          authorizationEndpoint: 'https://www.facebook.com/v2.8/dialog/oauth',
          scope: ['email'],
          clientId: '464053600441398'
         // clientId: '1744092205914260' //producción
          
        });
        // Facebook
        /*$authProvider.facebook({
          name: 'facebook',
          url: '/auth/facebook',
          authorizationEndpoint: 'https://www.facebook.com/v2.5/dialog/oauth',
          redirectUri: window.location.origin + '/',
          requiredUrlParams: ['display', 'scope'],
          scope: ['email'],
          scopeDelimiter: ',',
          display: 'popup',
          oauthType: '2.0',
          popupOptions: { width: 580, height: 400 }
        });*/


        $authProvider.twitter({
          url: '/auth/twitter'
        });

        $authProvider.oauth2({
          name: 'foursquare',
          url: '/auth/foursquare',
          clientId: 'MTCEJ3NGW2PNNB31WOSBFDSAD4MTHYVAZ1UKIULXZ2CVFC2K',
          redirectUri: window.location.origin || window.location.protocol + '//' + window.location.host,
          authorizationEndpoint: 'https://foursquare.com/oauth2/authenticate'
        });

        function skipIfLoggedIn($q, $auth) {
          var deferred = $q.defer();
          if ($auth.isAuthenticated()) {
            deferred.reject();
          } else {
            deferred.resolve();
          }
          return deferred.promise;
        }
        function loginRequired($q, $location, $auth) {
          var deferred = $q.defer();
          if ($auth.isAuthenticated()) {
            deferred.resolve();
          } else {
            $location.path('/login');
          }
          return deferred.promise;
        }
    });
})();
