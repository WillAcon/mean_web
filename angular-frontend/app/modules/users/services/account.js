(function() {

  'use strict';
  angular.module('module.users')
    .factory('Account', function($http) {
      return {
        getProfile: function() {
          /*var url = "/api/me?callback=JSON_CALLBACK";
          $http.jsonp(url).success(function(data) {
            console.log(data);
            return data;
          }.bind(this));*/

        /*  $http.jsonp(url)
          .success(function (data, status, headers, config) {
             $scope.details = data;
          })
          .error(function (data, status, headers, config) {
             $scope.statusval = status;
          });
          */
          return $http.get('/api/me');
        },
        updateProfile: function(profileData) {
          return $http.put('/api/me', profileData);
        },
        Client_id: function() {
          $http.get('/config/data').getProfile()
          .then(function(response) {
            console.log(response);
            //$scope.user = response.data;
            return response.client_id;
          })
          .catch(function(response) {
            //console.log('No autorizado');
            //toastr.error(response.data.message, response.status);
          });         
        }

      };
    });

}());