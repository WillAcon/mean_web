(function() {
  'use strict';
  /**
   * @ngdoc function
   * @name mean.module.users.controller:RegisterCtrl
   * @description Login Controller
   * @requires $location
   * @requires $auth
   * @requires toastr
   * Controller for Register Page
   **/
	angular
	  .module('module.users')
	  .controller('LogoutCtrl', function($location, $auth, toastr) {
	    if (!$auth.isAuthenticated()) { return; }
	    $auth.logout()
	      .then(function() {
	        toastr.info('You have been logged out bye');
	        $location.path('/');
	      });
	  });

}());