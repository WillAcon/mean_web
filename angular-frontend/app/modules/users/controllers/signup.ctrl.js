(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name mean.module.users.controller:RegisterCtrl
   * @description Login Controller
   * @requires $scope
   * @requires $routeParams
   * @requires $location
   * Controller for Register Page
   **/
  angular
    .module('module.users')
    .controller('SignupCtrl', function ($scope, $location, $auth, toastr, gettextCatalog) {
      $scope.user = {};
      $scope.signup = function() {
      $auth.signup($scope.user)
          .then(function(response) {
            $auth.setToken(response);
            $location.path('/');
            toastr.info(gettextCatalog.getString('You have successfully created a new account and have been signed-in'));
          })
          .catch(function(response) {
            toastr.error(response.data.message);
          });
      };
    });
})();
