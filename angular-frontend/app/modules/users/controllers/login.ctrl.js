(function() {
  'use strict';
  /**
   * @ngdoc function
   * @name mean.module.users.controller:LoginCtrl
   * @description Login Controller
   * @requires $scope
   * @requires $routeParams
   * @requires $location
   * Contrller for Login Page
   **/
  angular.module('module.users')
    .controller('LoginCtrl', function($scope,$routeParams, $location, $auth, toastr, gettextCatalog) {
      $scope.user = {};
      $scope.login = function() {
        $auth.login($scope.user)
          .then(function() {
            toastr.success('You have successfully signed in!');
            $location.path('/');
          })
          .catch(function(error) {
            toastr.error(error.data.message, error.status);
          });
      };
      $scope.authenticate = function(provider) {
        $auth.authenticate(provider)
          .then(function() {
            toastr.success('You have successfully signed in with ' + provider + '!');
            toastr.success(gettextCatalog.getString('Email'));
            $location.path('/');
          })
          .catch(function(error) {
            if (error.error) {
              // Popup error - invalid redirect_uri, pressed cancel button, etc.
              toastr.error(error.error);
            } else if (error.data) {
              // HTTP response error from server
              console.log('1');
              toastr.error(error.data.message, error.status);
            } else {
               console.log('1');
              toastr.error(error);
            }
          });
      };
    });

}());