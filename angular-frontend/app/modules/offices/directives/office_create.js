(function () {
  'use strict';
  /**
   * @ngdoc directive
   * @name mean.module.core.directive:login
   * @description
   * # login
   */
  angular
    .module('mean.module.offices')
    .directive('office_create', function () {
      return {
        templateUrl: 'modules/offices/views/create.html',
        restrict: 'E'
      };
    });

})();
