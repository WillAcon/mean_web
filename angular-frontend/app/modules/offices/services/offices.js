(function() {

  'use strict';
  angular.module('module.offices')
    .factory('Office', function($http) {
      return {
        getOffices: function() {
          return $http.get('/office/list');
        },
        getOffice: function(idOffice) {
          return $http.get('/office/list/'+idOffice);
        },
        updateOffice: function(officeData) {
          return $http.put('/office/update', officeData);
        },
        createOffice: function(officeData) {
          return $http.post('/office/create', officeData);
        }
      };
    });

}());