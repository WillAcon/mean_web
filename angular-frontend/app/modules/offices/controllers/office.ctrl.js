(function() {
  'use strict';
  /**
   * @ngdoc function
   * @name mean.module.users.controller:RegisterCtrl
   * @description Login Controller
   * @requires $scope
   * @requires $auth
   * @requires toastr
   * @requires Account
   * Controller for Register Page
   **/
  angular
    .module('module.offices')
    .controller('OfficeCtrl', function($scope, $auth, toastr, Account, Office) {
      //console.log(Office.getOffice());
      $scope.OfficeForm = {};
      $scope.getOffices = function() {
        Office.getOffices()
          .then(function(response) {
            $scope.offices = response.data;
           // console.log($scope.offices);
          })
          .catch(function(response) {
            toastr.error(response.data.message, response.status);
          });
      };
      $scope.createOffice = function() {
        //console.log($scope.OfficeForm);
       Office.createOffice($scope.OfficeForm)
          .then(function() {
            toastr.success('Office has been created '+$scope.OfficeForm.name);
            $scope.OfficeForm = {};
          })
          .catch(function(response) {
            toastr.error(response.data.message, response.status);
          });
      };

      $scope.getOffices();


    });

}());