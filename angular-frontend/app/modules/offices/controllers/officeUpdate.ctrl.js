(function() {
  'use strict';
  /**
   * @ngdoc function
   * @name mean.module.users.controller:RegisterCtrl
   * @description Login Controller
   * @requires $scope
   * @requires $auth
   * @requires toastr
   * @requires Account
   * Controller for Register Page
   **/
  angular
    .module('module.offices')
    .controller('OfficeUpdateCtrl', function($scope, $auth, toastr, Account, Office,$stateParams,$location) {
      //console.log(Office.getOffice($stateParams.officeId));
     // console.log($stateParams.officeId);
     // $scope.OfficeForm;
      $scope.getOffices = function() {
        Office.getOffices()
          .then(function(response) {
            $scope.offices_list = response.data;
            getSelect_List($scope.offices_list);
          })
          .catch(function(response) {
            toastr.error(response.data.message, response.status);
          });
      };
      $scope.getOffice = function() {
        Office.getOffice($stateParams.officeId)
          .then(function(response) {
//            console.log(response);
            if(response.data != '') {
              $scope.office = response.data; 
              $scope.getOffices();
            }else {
              $location.path('/office-list')
            }
          })
          .catch(function(response) {
            
            toastr.error(response.data.message, response.status);
          });
      };
      $scope.createOffice = function() {
       Office.createOffice($scope.OfficeForm)
          .then(function() {
            toastr.success('Office has been created '+$scope.OfficeForm.name);
            $scope.OfficeForm = {};
          })
          .catch(function(response) {
            toastr.error(response.data.message, response.status);
          });
      };
      $scope.updateOffice = function() {
        Office.updateOffice($scope.office)
          .then(function() {
            toastr.success('Office has been updated');
            $location.path('/office-list');
          })
          .catch(function(response) {
            toastr.error(response.data.message, response.status);
          });
      };
      $scope.selectCallback = selectCallback;
      $scope.getOffice();
      

      
        
      function selectCallback(_newValue, _oldValue)
      {
         // LxNotificationService.notify('Change detected');
           toastr.success("Guardado", _newValue,{closeButton: true,positionClass: "toast-bottom-center",progressBar: true});

         // console.log('Old value: ', _oldValue);
         // console.log('New value: ', _newValue);
         //console.log(_newValue);
          $scope.office.parent = _newValue;
      }
      function getSelect_List(offices_list) {

       
        var select_temp = search($scope.offices_list,$scope.office.parent);
        //console.log(select_temp);
        $scope.selectModel = {
            selectedPerson:select_temp,
        };

      }

      function search(array, key, prop){
        // Optional, but fallback to key['name'] if not selected
        prop = (typeof prop === 'undefined') ? '_id' : prop;    

        for (var i=0; i < array.length; i++) {
            if (array[i][prop] === key) {
                return array[i];
            }
        }
     }

      
      
     
      

    });

}());