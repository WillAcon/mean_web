(function () {
  'use strict';
  angular
    .module('module.offices')
    .config(function ($stateProvider, $authProvider) {
      $stateProvider
        .state('office-create', {
          url: '/office-create',
          templateUrl: 'modules/offices/views/create.html',
          controller: 'OfficeCtrl',
          resolve: {
            loginRequired: loginRequired
          }
        })
        .state('office-list', {
          url: '/office-list',      
          views:{
            '': {
              templateUrl: 'modules/offices/views/list.html',
                controller: 'OfficeCtrl',
                resolve: {
                  loginRequired: loginRequired
                }
             },
             'header': {
                            templateUrl: 'views/menu.html',
                            controller: 'NavbarCtrl'
                        }
          }
        })
        .state('editar', {
        url: "/office/:officeId/edit",
        templateUrl: 'modules/offices/views/edit.html',
        controller: 'OfficeUpdateCtrl',
        resolve: {
            loginRequired: loginRequired
        }
       });
       
        function loginRequired($q, $location, $auth) {
          var deferred = $q.defer();
          if ($auth.isAuthenticated()) {
            deferred.resolve();
          } else {
            $location.path('/login');
          }
          return deferred.promise;
        }
    });
})();
