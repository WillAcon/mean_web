(function () {
  'use strict';
  angular
    .module('module.admin')
    .config(function ($stateProvider, $urlRouterProvider) {

  
    
    $stateProvider.state('admin', {
      abstract: true,
      views: {
         header: {
           templateUrl: '/modules/admin/views/header.html',
           controller: 'NavbarCtrl'
         },
         
         main: {
           templateUrl: '/modules/admin/views/home.html',
           controller: 'HomeCtrl'
         },
         
        /* footer: {
           templateUrl: 'footer.html',
           controller: 'FooterController as Footer'
         }*/
      }
    })
    .state('admin.items', {
      url: '/admin/items',
      templateUrl: '/modules/admin/views/temp.html',
      controller: 'HomeCtrl'
    });
     /* $stateProvider
        .state('admin', {
          url: '/router',
          template: '<div class="lockscreen" style="height: 100%"></div>',
          controller: 'RouteCtrl'
        })
        .state('error', {
          url: '/error',
          template: '<div class="text-center alert alert-danger" style="margin: 100px">An error occurred.</div>'
        })
        .state('admin.dashboard', {
          abstract: true,
          url: '/admin',
          templateUrl: 'modules/core/views/app.html',
        //  controller: 'MainCtrl'
        })
        .state('admin.home', {
          url: '',
          templateUrl: 'modules/core/views/home.html',
          controller: 'HomeCtrl'
        });*/

      //$urlRouterProvider.otherwise('/404');
    });

})();
