(function () {
  'use strict';
  /**
   * @ngdoc overview
   * @name mean.module.core
   * @module
   * @description
   * @requires loopbackApp
   *
   * The `mean.module.core` module provides services for interacting with
   * the models exposed by the LoopBack server via the REST API.
   *
   */
  angular.module('module.admin', ['gettext']);

})();
