var mongoose = require('mongoose')
  , Schema = mongoose.Schema;
  //, AutoIncrement = require('mongoose-sequence');

var photoSchema = new Schema({
  title: { type : String, length   : 255 },
  public_id: {type: String},
  image      : { type : JSON}
});

//officeSchema.plugin(AutoIncrement);

module.exports = mongoose.model('Photo', photoSchema);

