var mongoose = require('mongoose')
  , Schema = mongoose.Schema;
  //, AutoIncrement = require('mongoose-sequence');

var officeSchema = new Schema({
  code: String,
  name: String,
  //parent: String,
  parent:{type: Schema.Types.ObjectId, ref: 'Office'},
  weight: {type: Number, default: 0}
});

//officeSchema.plugin(AutoIncrement);

module.exports = mongoose.model('Office', officeSchema);


