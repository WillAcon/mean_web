var mongoose = require('mongoose')
	, Office = mongoose.model('Office')
	, request = require('request')
	, qs = require('querystring');

var config = global.config; //get config site

/*exports.list = function(req, res) {
	Team.find({}, function(err, teams) {
		res.json(teams);
		console.log(teams);
	});
}


exports.update = function(req, res){
	var team = new Team(req.body);
	Team.update({ _id: team.id }, {votes: team.votes}, function (err, numberAffected, raw) {
		var socketIO = global.socketIO; 
		socketIO.sockets.emit('team:updated', team);
		res.json(true);
	});
}*/

/*
 |--------------------------------------------------------------------------
 | Create Email and Password Account
 |--------------------------------------------------------------------------
 */
exports.create = function(req, res) {
	console.log(req.body);
   var office = new Office({
      code: req.body.code,
      name: req.body.name,
      parent: req.body.parent
   });
   office.save(function(err, result) {
       //if(err) throw err;
       if (err) {
         res.status(500).send({ message: err.message });
       }
       res.status(200).end();
   });
}

exports.list = function(req, res) {
	 var param = req.params;
     Office.find({}, function(err, offices) {
		Office.populate(offices, {path: "parent"},function(err, offices){
            res.json(offices);
        }); 
	});
}
exports.getData = function(req, res) {
	var param = req.params;
	Office.findOne({_id:param.id}, function(err, offices) {
		res.json(offices);
	});
}
exports.update = function(req, res) {
	
	  Office.findById(req.body, function(err, office) {
	      if (!office) {
	        return res.status(400).send({ message: 'Office not found' });
	      }
	      office.name = req.body.name || office.name;
	      office.code = req.body.code || office.code;
	      office.parent = req.body.parent || office.parent;
	      office.save(function(err) {
	        res.status(200).end();
	      });
    });
}