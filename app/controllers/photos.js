var mongoose = require('mongoose')
	, Photo = mongoose.model('Photo')
	, request = require('request')
	, qs = require('querystring')
	, cloudinary = require('cloudinary').v2;

var config = global.config; //get config site

User = mongoose.model('User');

// only keep track of most recent messages
var messageQueue = (function(size){
  var queue = [];

  queue.push = function(a) {
    if(this.length >= size) this.shift();
    return Array.prototype.push.apply(this, arguments);
  };

  return queue;
})(15);

/*
 |--------------------------------------------------------------------------
 | Create Email and Password Account
 |--------------------------------------------------------------------------
 */
exports.create = function(req, res) {

  var imageFile = req.files.file.path;
  // Upload file to Cloudinary
  //cloudinary.uploader.upload(imageFile, {tags: req.body.tags, context: 'caption=' + req.body.title})
  cloudinary.uploader.upload(imageFile, {upload_preset: "demoapp", tags: req.body.tags, context: req.body.options })
  //{ "alt" => "My image", "caption" => "Profile Photo" }
      .then(function (image) {
       // console.log('** file uploaded to Cloudinary service');
         var photo = new Photo({
		      title: req.body.options.caption,
		      public_id: image.public_id,
		      image: image
		 });
		 //console.log(image);
		 photo.save(function(err, result) {
	       if (err) {
	         res.status(500).send({ message: err.message });
	       }
         //socket1.emit('guardarfoto', { photo : result});
	      // res.status(200).end();
           res.json(photo);
	     });
      })
      .then(function (photo) {
        console.log('** photo saved')
      })
      .finally(function () {
         //res.render('photos/create_through_server', {photo: photo, upload: photo.image});
      });
}

exports.list = function(req, res) {
  //limit: 5
	 var param = req.query;
   //console.log(param);

   Photo.find({}, function(err, photos) {
		//Photo.populate(photos, {path: "_id"},function(err, photos){
         res.jsonp(photos);
     //   });
	 }).sort({_id : -1}).skip(param.skip).limit(param.limit);

}

exports.list_photos = function(socket) {

   Photo.find({}, function(err, photos) {
        Photo.populate(photos, {path: "parent"},function(err, photos) {
          socket.emit('list',{ photos : photos}); // broadcast updated item.
        });
   });

}

exports.GuardarData =  function(socket, data) {
   //  socket.emit('list',{ photos : photos}); // broadcast updated item.
  //messageQueue.push(data);
socket.emit('guardarfoto', data);
console.log(data);
  /*  Photo.find({}, function(err, photos) {
      socket.broadcast.emit('guardarfoto', photos);
      //console.log(photos);
   });*/


}

// broadcast user's message to other users
exports.sendMessage = function(socket, user, data) {
  if(user.logged_in === false) {
    return socket.emit('unauthorized', {
      messages: messageQueue
    });
  }

  messageQueue.push({
    author: user,
    body:   data.body,
    date:   Date.now()
  });
  socket.broadcast.emit('message', messageQueue);
};


// send the user on connection
exports.initUser = function(socket) {
  socket.broadcast.emit('init', {
    user: {nam2:"will"}
  });
};


/*exports.getData = function(req, res) {
	var param = req.params;
	Photo.findOne({_id:param.id}, function(err, photos) {
		res.json(photos);
	});
}
exports.update = function(req, res) {
	
	  Photo.findById(req.body, function(err, office) {
	      if (!office) {
	        return res.status(400).send({ message: 'Photo not found' });
	      }
	      office.name = req.body.name || office.name;
	      office.code = req.body.code || office.code;
	      office.parent = req.body.parent || office.parent;
	      office.save(function(err) {
	        res.status(200).end();
	      });
    });
}*/