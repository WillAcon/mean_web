
var path = require('path')
  , rootPath = path.normalize(__dirname + '/..')
  , dotenv = require('dotenv').config({path: rootPath+'/.env'});
  

module.exports = {
  development: {
    //db: 'mongodb://localhost/todos',
    root: rootPath,
    root_path: rootPath + '/angular-frontend/app',
    app: {
      name: 'MEAN DEV',
      root_path: '/angular-frontend/app',
    },
     // App Settings
    MONGO_URI: 'mongodb://localhost/login4',
    TOKEN_SECRET: 'YOUR_UNIQUE_JWT_TOKEN_SECRET',
    // OAuth 2.0
    FACEBOOK_SECRET: '4b3557e98465dc0302a140043a2832d2',
    CLIENT_ID: '464053600441398',
    // OAuth 1.0
    TWITTER_KEY: ' twDM0tOOrCgVP29PUl2xJJUtO',
    TWITTER_SECRET: '20HYU90grSguBoqI8RFmoi8JmABz9GYUN6cv00LkVBBptp1iSn',
  },
  test: {
    db: 'mongodb://localhost/todos',
    root: rootPath + '/angular-frontend/app',
    app: {
      name: 'MEAN TEST',
      root_path: '/angular-frontend/app',
    }
  },  
  production: {
   // db: 'mongodb://test:test@widmore.mongohq.com:10000/vote-express',
    root: rootPath,
    root_path: rootPath + '/angular-frontend/dist',
    app: {
      name: 'MEAN PROD',
      root_path: '/angular-frontend/dist',
    },
         // App Settings
    MONGO_URI: process.env.MONGO_URI || 'mongodb://localhost/login4',
    TOKEN_SECRET: process.env.TOKEN_SECRET || 'YOUR_UNIQUE_JWT_TOKEN_SECRET',
    // OAuth 2.0
    FACEBOOK_SECRET: process.env.FACEBOOK_SECRET || '4b3557e98465dc0302a140043a2832d2',
    CLIENT_ID: '1744092205914260',
    // OAuth 1.0
    TWITTER_KEY: process.env.TWITTER_KEY || ' twDM0tOOrCgVP29PUl2xJJUtO',
    TWITTER_SECRET: process.env.TWITTER_SECRET || '20HYU90grSguBoqI8RFmoi8JmABz9GYUN6cv00LkVBBptp1iSn',
  },
}
