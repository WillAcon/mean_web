var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var jwt = require('jwt-simple');
var moment = require('moment');
var async = require('async');
var qs = require('querystring');
var bcrypt = require('bcryptjs');

/*var io = require('socket.io');
var server = require('http').Server(express);
var io = require('socket.io').listen(server);*/

var path = require('path');

var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();


var config = global.config;
bodyParser.urlencoded({ extended: true });
//Controllers
var users = require('../app/controllers/users');
var offices = require('../app/controllers/offices');
var photos = require('../app/controllers/photos');

 //router demo
/*router.get('/user/status', function(req, res) {
  res.status(200).json({
    status: true
  });
});*/

/*router.get('/*', function (req, res, next) {
  if (req.url.indexOf("/images/") === 0 || req.url.indexOf("/stylesheets/") === 0) {
    res.setHeader("Cache-Control", "public, max-age=2592000");
    res.setHeader("Expires", new Date(Date.now() + 2592000000).toUTCString());
  }
  next();
});
*/


router.get('/admin', function(req, res) {
    res.sendFile(path.join(config.root_path +'/admin.html'));
     //res.sendFile(path.join(config.root_path +'/../dist/admin.html'));
});
router.get('/client', function(req, res) {
    res.sendFile(path.join(config.root_path +'/client.html'));
});

  //router teams
  /*router.get('/teams', teams.list);
  router.put('/teams/:id', teams.update);

  //router Users
  router.get('/user/list', users.list);
  router.get('/user/status', users.status);
  router.post('/user/register',jsonParser, users.registrar);
  router.post('/user/login', jsonParser, users.login);
  router.get('/user/logout', jsonParser, users.logout);
 // router.put('/users/:id', users.update);*/

/*
  |------------------------------------------------------------------------
  | Router User
  |------------------------------------------------------------------------
*/
 //router.get('/config/data', jsonParser,users.mydata);
  router.get('/config/data', function(req, res) {
    res.status(200).json({
      client_id: config.CLIENT_ID
    });
  });
 router.post('/auth/login', jsonParser, users.loginEmail);
 router.post('/auth/signup', jsonParser, users.signupEmail);
 router.post('/auth/facebook', jsonParser, users.loginFB);
 router.post('/auth/twitter', jsonParser, users.loginTW);
 router.post('/auth/unlink', jsonParser, ensureAuthenticated,users.unlink); 
 router.get('/api/me', jsonParser, ensureAuthenticated,users.mydata);
 router.put('/api/me', jsonParser, ensureAuthenticated,users.update);

/*
  |------------------------------------------------------------------------
  | Router Office
  |------------------------------------------------------------------------
*/
 router.post('/office/create', jsonParser, offices.create);
 router.get('/office/list', jsonParser, offices.list);
 router.get('/office/list/:id', offices.getData);
 router.put('/office/update', jsonParser,offices.update);

 /*
  |------------------------------------------------------------------------
  | Router Photos
  |------------------------------------------------------------------------
*/

// start socketio connection
  router.post('/photo/create', multipartMiddleware, photos.create);
  router.get('/photo/list', photos.list);
//io.on('connection', function (socket) { 
  



//});

 //router.post('/photo/create', multipartMiddleware, photos.create);
// router.get('/photo/list', photos.list);
 /*router.get('/photo/list/:id', photos.getData);
 router.put('/photo/update', jsonParser,photos.update);*/



/*
 |--------------------------------------------------------------------------
 | Function - Login Required Middleware
 |--------------------------------------------------------------------------
 */
function ensureAuthenticated(req, res, next) {
  if (!req.header('Authorization')) {
    return res.status(401).send({ message: 'Please make sure your request has an Authorization header' });
  }
  var token = req.header('Authorization').split(' ')[1];

  var payload = null;
  try {
    payload = jwt.decode(token, config.TOKEN_SECRET);
  }
  catch (err) {
    return res.status(401).send({ message: err.message });
  }

  if (payload.exp <= moment().unix()) {
    return res.status(401).send({ message: 'Token has expired' });
  }
  req.user = payload.sub;
  next();
}


module.exports = router;