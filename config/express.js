
/**
 * Module dependencies.
 */

var express = require('express')
  , session = require('express-session')
  , mongoStore = require('connect-mongo')(session)
  , flash = require('connect-flash')
  , favicon = require('static-favicon')

    //io = require('socket.io'),
  , hash = require('bcrypt-nodejs')
  , http = require('http')
    //server = http.createServer(app),
    //io = io.listen(server),
  , path = require('path')
  , morgan = require('morgan')
  , cookieParser = require('cookie-parser')
  , bodyParser = require('body-parser')
  , passport = require('passport')
  , localStrategy = require('passport-local' ).Strategy;

module.exports = function (app, config,routes) {


   app.set('showStackError', true);
    // should be placed before express.static
    /*app.use(express.compress({
      filter: function (req, res) {
        return /json|text|javascript|css/.test(res.getHeader('Content-Type'));
      },
      level: 9
    }))*/
    // for dev
    app.use(express.static(config.root + '/angular-frontend/app/'));

    // for production, do 'grunt --force' and then comment the line above
    // and uncomment the line below

    ///app.use(express.static(config.root +  '/public'));

    /// catch 404 and forwarding to error handler
    /*app.use(function (req, res, next) {
      var err = new Error('Not Found');
      err.status = 404;
      next(err);
    });*/


    // don't use logger for test env
    if (process.env.NODE_ENV !== 'test') {
      //app.use(express.logger('dev'))
    }

    // set views path, template engine and default layout
    app.set('views', config.root + '/app/views');
    app.set('view engine', 'jade');
    //console.log(config);

    //Start configure
   // app.configure(function () {

    // middleware settings
    app.use(favicon());
    /*app.use(morgan('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(cookieParser());
    app.use(require('stylus').middleware(path.join(config.root, 'public')));*/

    // cookieParser should be above session
    //app.use(express.cookieParser())

    // bodyParser should be above methodOverride
    //app.use(express.bodyParser())
  //  app.use(methodOverride())

    // express/mongo session storage
  /* app.use(session({
       secret: 'noobjs',
       store: new mongoStore({
         url: config.db,
         collection : 'sessions'
       })
    }));*/
    /*app.use(require('express-session')({
        secret: 'keyboard cat',
        resave: false,
        saveUninitialized: false
    }));*/
    // connect flash for flash messages
    ///app.use(flash());
    // user schema/model
    var User = require('../app/models/user.js');

    /*app.use(passport.initialize());
    app.use(passport.session());
    // configure passport
    passport.use(new localStrategy(User.authenticate()));
    passport.serializeUser(User.serializeUser());
    passport.deserializeUser(User.deserializeUser());*/


    //ADD
    // define middleware
    app.use(express.static(path.join(__dirname, '../client')));
    app.use(morgan('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(cookieParser());
    app.use(require('express-session')({
        secret: 'keyboard cat',
        resave: false,
        saveUninitialized: false
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(express.static(path.join(__dirname, 'public')));

    // configure passport
    passport.use(new localStrategy(User.authenticate()));
    passport.serializeUser(User.serializeUser());
    passport.deserializeUser(User.deserializeUser());


    // routes should be at the last
    app.use(routes);

    // routes
    //app.use('/', router);
    // assume "not found" in the error msgs
    // is a 404. this is somewhat silly, but
    // valid, you can do whatever you like, set
    // properties, use instanceof etc.
    app.use(function(err, req, res, next){
      // treat as 404
      if (~err.message.indexOf('not found')) return next();

      // log it
      console.error(err.stack);

      // error page
      res.status(500).render('500', { error: err.stack });
    })

    // assume 404 since no middleware responded
    app.use(function(req, res, next){
      res.status(404).render('404', { url: req.originalUrl, error: 'Not found' });
    })

    // development error handler
    // will print stacktrace
    if (app.get('env') === 'development') {
      app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
          message: err.message,
          error: err
        });
      });
    }

    // production error handler
    // no stacktraces leaked to user
    app.use(function (err, req, res, next) {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: {}
      });
    });

//  })//end configure
}
