/**
 * Module dependencies.
 */
var path = require('path'),
    async = require('async'),
    bcrypt = require('bcryptjs'),
    bodyParser = require('body-parser'),
    colors = require('colors'),
    cors = require('cors'),
    express = require('express'),
    logger = require('morgan'),
    favicon = require('static-favicon'),
    mongoose = require('mongoose'),
    fs = require('fs')
    compression = require('compression');

var path_env = path.join(__dirname, '/'); // URL PAGE 
var dotenv = require('dotenv').config({path: path_env+'/.env'});

// Load configurations
// if test env, load example file
var env = process.env.NODE_ENV || 'development'
  , config = require('./config/config')[env]
  , mongoose = require('mongoose');

global.config = config;

// Bootstrap db connection
mongoose.connect(config.MONGO_URI);

// Bootstrap models
var models_path = __dirname + '/app/models';
fs.readdirSync(models_path).forEach(function (file) {
  require(models_path+'/'+file);
})

//mongoose.connect(config.MONGO_URI);
mongoose.connection.on('error', function(err) {
  console.log('Error: Could not connect to MongoDB. Did you forget to run `mongod`?'.red);
});

var app = express();

/*
// Bootstrap routes
var routes = require('./config/routes');

app.use(routes);
*/

//app.use(sockets);
/*
app.set('port', process.env.PORT || 5000);
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
*/

//--------

//app.configure(function(){
  // Bootstrap routes
  var routes = require('./config/routes');
  //console.log(process.env.PORT);
  app.set('port', process.env.PORT || 5000);
  app.set('views', __dirname + '/views');
  app.set("jsonp callback", true);
  app.set('view engine', 'jade');
  //app.use(favicon());
  app.use(favicon(__dirname + config.app.root_path +'/favicon.ico')); 
  app.use(logger('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  //app.use(methodOverride());
  app.use(routes);
  //app.use(express.static(path.join(__dirname, 'public')));
//});

//-----------

var oneDay = 2628000000;
app.use(compression({filter: shouldCompress})); //cache


// Use compress middleware to gzip content


// Force HTTPS on Heroku
/*if (app.get('env') === 'production') {
  app.use(function(req, res, next) {
    var protocol = req.get('x-forwarded-proto');
    protocol == 'https' ? next() : res.redirect('https://' + req.hostname + req.url);
  });
}*/

console.log(config);
  app.use(express.static(path.join(__dirname, '/angular-frontend/dist'), { maxAge: oneDay }));
  //config.app.root_path
  //app.use('/static', express.static(__dirname + config.app.root_path));
  ///app.set('view engine', 'jade');
//app.use(express.static(path.join(__dirname, '/public')));

/********
 * Angularjs Router and Nodejs
 ********/
 
// serve index and view partials
app.get('/', function(req, res){
  res.sendfile(config.root_path+'/index.html');
});
// redirect all others to the index (HTML5 history)
app.get('*',  function(req, res){
  res.sendfile(config.root_path+'/index.html');
});

/*
 |--------------------------------------------------------------------------
 | Start the Server
 |--------------------------------------------------------------------------
 */
var server = app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + app.get('port'));
});

var sockets = require('./sockets/socket')(server);

function shouldCompress (req, res) {
  if (req.headers['x-no-compression']) {
    // don't compress responses with this request header
    return false
  }
  // fallback to standard filter function
  return compression.filter(req, res)
}