# MEAN site web and system - Web-Out S.A.
## [View demo](http://mean.web-out.com/)

Sample AngularJS + NodeJS/Express application demonstrating the use of
Socket.IO.  I am using:

* [AngularJS](http://angularjs.org)
* [NodeJS](http://nodejs.org)
* [Cloudinary](http://cloudinary.com/)
* [Express](http://expressjs.com)
* [Satellizer](https://github.com/sahat/satellizer)
* [Socket.IO and Socket.IO Client](http://socket.io) for web socket
  support
* [Brian Ford's Angular Socket IO
  client](https://github.com/btford/angular-socket-io)
* [Lumx](http://ui.lumapps.com)
  [Express](https://www.npmjs.org/package/generator-express) and
  [AngularJS](https://www.npmjs.org/package/generator-angular)


This project is based on MEAN, also available socket and cloudinary for the image and / or files.

![alt text](http://res.cloudinary.com/web-out-s-a/image/upload/v1480716462/mean_wo_s76lbh.png "Login")


## Runtime instructions

I have the root `server.js` node script pointing to serve / as the content
in /angular-frontend/app/ - you can construct and build and minify with
`grunt --force` in the `angular-frontend` directory. This will set the
content in `/public` to the minified version of the application and you
can serve that by editing `server.js`.

To launch the app and run in development mode: 

```
node server.js
```

Once you're ready to test a final build (minified, two javascript files), do this:

1.  Edit the `server.js` file and swap the commented and uncommented lines that
serve the static content.


2.  Run the build:
```
cd angular-frontend
npm install
bower install or bower install --allow-root
cd ..
node server.js
```

3. Browse to `http://localhost:5000` and you should see the site.
cd angular-frontend
grunt

## Cloudninary configuration
1. Change `sample.env` file by `.env`.

2. You data configuration is in [Cludinary console](https://cloudinary.com/console) `Environment variable:`

## Authenticate configuration (Satellizer)
1. The `config.js` file configuration is in `config/`.
```
FACEBOOK_SECRET,
TWITTER_KEY,
TWITTER_SECRET,
```

## Site in production
1. 
```
cd angular-frontend
grunt
cd..
```
2. Change in `server.js` file main `angular-frontend/app` by `angular-frontend/dist`

3. Browse to `http://localhost:5000` and you should see the site
client using minified scripts.


---
###### sponsor: [WEB-OUT S.A.](http://web-out.com)